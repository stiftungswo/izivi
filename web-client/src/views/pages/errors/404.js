import Inferno from 'inferno';
import Card from '../../tags/card';

export default function () {
	return (
		<div className="page page__404">
			<Card>
				<h1>404 Page</h1>
				<p>Die angeforderte Seite konnte nicht gefunden werden.</p>
			</Card>
		</div>
	);
}
