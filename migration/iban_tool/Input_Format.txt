Export the no_iban table and create a csv as follows:
the row_id is just a row counter with 6 leading zeroes (can be set in calc/excel cell format)

[row_id];[zdp];[Bank Clearing-Nr];[Old_Account_Number];

Example:
000001;1;8271;137279.001.11;
000002;10705;700;1132-0150.533;



Command:
java -jar ./ibantool_java.jar -a -i "D:/izivi/migration/iban_tool/no_iban_export_2017.09.01.csv” -o "D:/izivi/migration/iban_tool/output.csv" -g 